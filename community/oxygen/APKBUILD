# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=oxygen
pkgver=5.23.1
pkgrel=0
pkgdesc="Artwork, styles and assets for the Oxygen visual style for the Plasma Desktop"
# armhf blocked by extra-cmake-modules
# s390x, mips64 and riscv64 blocked by polkit -> kcmutils
arch="all !armhf !s390x !mips64 !riscv64"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-or-later"
makedepends="
	extra-cmake-modules
	frameworkintegration-dev
	kcmutils-dev
	kcompletion-dev
	kconfig-dev
	kdecoration-dev
	kguiaddons-dev
	ki18n-dev
	kservice-dev
	kwayland-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	xcb-util-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/oxygen-$pkgver.tar.xz"
subpackages="$pkgname-lang $pkgname-sounds::noarch"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sounds() {
	pkgdesc="$pkgdesc (sounds)"

	amove usr/share/sounds
}
sha512sums="
cf1cbae0c14189382bfa2e7b4b20ffd8457ed2545da4429b4f9b5417da4f23d6e1b3e9081b772c8c32f49947f4fa38c0778084c4260d3ab090f624bcade3aa28  oxygen-5.23.1.tar.xz
"
