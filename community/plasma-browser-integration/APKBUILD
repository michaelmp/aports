# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-browser-integration
pkgver=5.23.1
pkgrel=0
pkgdesc="Components necessary to integrate browsers into the Plasma Desktop"
# armhf blocked by extra-cmake-modules
# s390x, mips64 and riscv64 blocked by polkit -> kio
# ppc64le blocked by kaccounts-integration
arch="all !armhf !s390x !mips64 !riscv64 !ppc64le"
url="https://community.kde.org/Plasma/Browser_Integration"
license="GPL-3.0-or-later"
makedepends="
	extra-cmake-modules
	kactivities-dev
	kconfig-dev
	kdbusaddons-dev
	kfilemetadata-dev
	ki18n-dev
	kio-dev
	knotifications-dev
	krunner-dev
	plasma-workspace-dev
	purpose-dev
	qt5-qtbase-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-browser-integration-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
084437387f9645007e552d3b33b4b20c593023c109589bcc7efc2543bd8caec385e5900034822f322e4dd7f3f904d8a1f22ba3928d4ba3e977b35409a1677977  plasma-browser-integration-5.23.1.tar.xz
"
